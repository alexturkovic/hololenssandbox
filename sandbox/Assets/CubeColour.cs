﻿using Microsoft.MixedReality.Toolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CubeColour : MonoBehaviour
{
    // A flag to indicate if an action (like dragging/dropping a piece) is in progress
    private bool interaction_in_progress = false;

    // Default ULD wireframe colour and its highlight
    private Color default_parent_renderer_colour = Color.red;
    private Color highlight_parent_renderer_colour = Color.yellow;
    
    // Parent ULD
    private Color cuboidColor;
    private Renderer cuboids_parent_renderer;
    private Outline cuboids_parent_outline_highlight;

    // Destination ULD
    private Renderer cuboids_destination_ULD_wireframe_renderer;
    private Outline cuboids_destination_ULD_wireframe_outline;
    
    // The cuboid (item/piece)
    public Color CuboidColor { get => cuboidColor; set => cuboidColor = value; }

    /// <summary>
    /// Used when updating the colour of the cuboid/item/piece for weight or profit.
    /// </summary>
    /// <param name="newColor"></param>
    public void UpdateColor(Color newColor)
    {
        cuboidColor = newColor;
        this.GetComponent<Renderer>().material.color = cuboidColor;
    }

    /// <summary>
    /// Reverts the colour of the item/piece.
    /// </summary>
    public void RevertColor()
    {
        this.GetComponent<Renderer>().material.color = cuboidColor;

        // helpful 
        // Debug.Log("Focus OFF");

        if (CoreServices.InputSystem.GazeProvider.GazeTarget)
        {
            Debug.Log("User gaze is currently over game object: " + CoreServices.InputSystem.GazeProvider.GazeTarget);
        }

        //Debug.Log("Gaze is looking in direction: " + CoreServices.InputSystem.GazeProvider.GazeDirection);

        //Debug.Log("Gaze origin is: " + CoreServices.InputSystem.GazeProvider.GazeOrigin);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        // note: following line removed as it was causing issues on setting the weight and profit filter colours
        // we are now setting the cuboid colour on instantiation through the CubeDataColours setRandomColours method.
        //SetRandomColor();
    }

    public void SetRandomColor()
    {
        cuboidColor = new Color(Random.Range((float)0.0, (float)1.0), Random.Range((float)0.0, (float)1.0), Random.Range((float)0.0, (float)1.0));
        this.GetComponent<Renderer>().material.color = cuboidColor;
    }

    /// <summary>
    /// 
    /// </summary>
    public void HighlightCuboid()
    {
        try
        {
            this.GetComponent<Renderer>().material.color = new Color(1f, 0f, 0f, 0.70f);
        }
        catch(Exception e)
        {

        }
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            if(interaction_in_progress)
            {
                GameObject[] all_uld_containers = GameObject.FindGameObjectsWithTag("ULD");

                // get the bounds of the current selected pieces parent container
                Collider pieces_parent_container_collider = this.transform.parent.gameObject.GetComponent<Collider>();

                if (!pieces_parent_container_collider.bounds.Contains(this.transform.position)) //if the piece is no longer in its parent container
                {
                    foreach (GameObject container in all_uld_containers) // then go through ALL the containers
                    {
                        Collider parent_collider = container.GetComponent<Collider>(); // get the collder of the container

                        if (parent_collider.bounds.Contains(this.transform.position)) // check if the current piece/item is in that container
                        {
                            foreach (Transform child in container.transform) // if yes then go through that containers children
                            {
                                // Get the uld wireframe
                                if (child.name.StartsWith("uld")) //https://stackoverflow.com/questions/46103030/how-can-i-find-child-objects-of-parent-object-by-tag
                                {
                                    // make it yellow
                                    cuboids_destination_ULD_wireframe_renderer = child.GetComponent<Renderer>();
                                    cuboids_destination_ULD_wireframe_renderer.material.color = highlight_parent_renderer_colour;

                                    // and highlight it
                                    cuboids_destination_ULD_wireframe_outline = child.GetComponent<Outline>();
                                    cuboids_destination_ULD_wireframe_outline.enabled = true;
                                }
                            }
                        }
                        else
                        {
                            // If it's not the item/pieces parent that is the current 'container'
                            if(!container.Equals(this.transform.parent.gameObject))
                            {
                                // then check if its red, if its not, make it red
                                foreach (Transform child in container.transform) // if yes then go through that containers children
                                {
                                    // Get the uld wireframe
                                    if (child.name.StartsWith("uld")) //https://stackoverflow.com/questions/46103030/how-can-i-find-child-objects-of-parent-object-by-tag
                                    {
                                        // make it red
                                        Renderer child_renderer = child.GetComponent<Renderer>();
                                        child_renderer.material.color = default_parent_renderer_colour;

                                        Outline child_outline = child.GetComponent<Outline>();
                                        if(child_outline.enabled)
                                        {
                                            child_outline.enabled = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }   
        catch(Exception e)
        {

        }
    }

    /// <summary>
    /// Intended to be executed when the user interacts with the cuboid on the 'On Manipulation Started' event. Highlights the parent ULD and any ULD the 
    /// item is dragged into.
    /// </summary>
    public void HighlightParent()
    {
        GameObject item_parent = this.transform.parent.gameObject;

        //Transform item_parent_transform = item_parent.transform;
        //item_parent_transform.Find()

        interaction_in_progress = true; // this flag is used for performance reasons to execute the code in 'Update()'

        if(item_parent.name == "Staging Area")
        {
            cuboids_parent_renderer = item_parent.GetComponent<Renderer>();
            cuboids_parent_renderer.material.color = highlight_parent_renderer_colour;
        }
        else
        {
            foreach (Transform child in item_parent.transform)
            {
                if (child.name.StartsWith("uld")) //https://stackoverflow.com/questions/46103030/how-can-i-find-child-objects-of-parent-object-by-tag
                {
                    cuboids_parent_renderer = child.GetComponent<Renderer>();
                    cuboids_parent_renderer.material.color = highlight_parent_renderer_colour;

                    // Give it a glow
                    cuboids_parent_outline_highlight = child.GetComponent<Outline>();
                    cuboids_parent_outline_highlight.enabled = true;
                }
            }
        }
    }

    /// <summary>
    /// Executed when the user finishes interacting with the cuboid, either in the 'Actions.ReturnItemToIntialPosition()' method or the modal event handler methods. 
    /// The cuboids_parent_renderer needs to be set before this is run, as set by 'HighlightParent()'.
    /// </summary>
    public void UnhighlightParent()
    {
        try
        {
            if (this.cuboids_parent_renderer != null)
            {
                this.cuboids_parent_renderer.material.color = default_parent_renderer_colour;

                if(cuboids_parent_outline_highlight != null)
                {
                    this.cuboids_parent_outline_highlight.enabled = false;
                }
            }

            if (this.cuboids_destination_ULD_wireframe_renderer != null)
            {
                this.cuboids_destination_ULD_wireframe_renderer.material.color = default_parent_renderer_colour;
                this.cuboids_destination_ULD_wireframe_outline.enabled = false;
            }

            this.interaction_in_progress = false;
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}
