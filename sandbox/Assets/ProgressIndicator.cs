﻿using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProgressIndicator : MonoBehaviour
{
    private GameObject GO_mainMessage;
    private GameObject GO_progress;
    private GameObject GO_subText;

    /// <summary>
    ///        TEXT              -- main_text
    ///  [========      ]        -- progressBar
    ///        TEXT              -- subBar_text
    /// </summary>
    private TextMeshPro main_text;
    private ProgressIndicatorLoadingBar progressBar;
    private TextMeshPro subBar_text;
    

    /// <summary>
    /// Initiales the progress indicator (with text, if passed to init). i.e. creates the indicator from the prefab.
    /// </summary>
    public void Init(string initMainText = "", string initSubText = "")
    {
        try
        {
            GO_progress = GameObject.Instantiate(Resources.Load("ProgressIndicatorLoadingBar_Solving") as GameObject);
            GO_mainMessage = GameObject.Find("MessageText");
            main_text = GO_mainMessage.GetComponent<TextMeshPro>();
            progressBar = GO_progress.GetComponent<ProgressIndicatorLoadingBar>();
            GO_subText = GameObject.Find("ProgressText");
            subBar_text = GO_subText.GetComponent<TextMeshPro>();

            // set bar to 0
            main_text.text = initMainText;
            progressBar.Progress = 0;
            subBar_text.text = initSubText;
        }
        catch(Exception e)
        {
            Debug.Log("Could not initialise the progress bar: "+e.Message);
        }
    }

    /// <summary>
    /// Text displayed ABOVE the loading bar.
    /// </summary>
    public void UpdateMainText(string mainText)
    {
        main_text.text = mainText;
    }

    /// <summary>
    /// Text displayed BELOW the loading bar.
    /// </summary>
    public void UpdateSubText(string subText)
    {
        subBar_text.text = subText;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="progress"></param>
    public void UpdateProgress(float progress)
    {
        progressBar.Progress = progress;
    }

    internal void Destroy(float delay)
    {
        Destroy(GO_progress, delay);
    }
}
