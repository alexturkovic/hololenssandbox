﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
    [SerializeField]
    private int weight;

    [SerializeField]
    private int offload_penalty;

    [SerializeField]
    private string piece_unique_name;

    public int Weight { get => weight; set => weight = value; }
    public int Offload_penalty { get => offload_penalty; set => offload_penalty = value; }
    public string Piece_unique_name { get => piece_unique_name; set => piece_unique_name = value; }
}
