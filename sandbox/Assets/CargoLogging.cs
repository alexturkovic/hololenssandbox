﻿using Assets;
using Microsoft.MixedReality.Toolkit;
using System;
using System.IO;
using UnityEngine;
//using Windows.Storage;

public class CargoLogging : MonoBehaviour
{
    private Messaging messaging;

    /*
    private StorageFolder folder;
    private StorageFile file;

    private string path = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/Documents/HoloLens/";
    private StreamWriter logWriter;
    */

    // Start is called before the first frame update
    async void Start()
    {
        /*
        string dmy = DateTime.Now.ToString("yyyyMMdd");
        string hourMinute = DateTime.Now.ToString("HHmm");
        string filename = "Session_" + dmy + "_" + hourMinute + ".log";
        */

        /*
        folder = await DownloadsFolder.CreateFolderAsync("HoloLens");
        file = await folder.CreateFileAsync(filename);

        FileInfo file = new FileInfo(path);
        file.Directory.Create(); // If the directory already exists, this method does nothing.

        string filename = "Session_" + dmy + "_" + hourMinute + ".log";
        
        logWriter = new StreamWriter(folder.Path + filename, true);
        logWriter.AutoFlush = true;
        */

        try
        {
            messaging = GameObject.Find("NearMenu4x2").GetComponent<Messaging>();
        }
        catch (Exception e)
        {
            Debug.Log("error trying to get the messagin script off the NearMenu4x2");
        }
    }

    public void Log(string text)
    {
        try
        {
            messaging.SendLoggingMessage(text);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    /// <summary>
    /// Called as part of the Interactable script on the CuboidPrefabNoTheme
    /// </summary>
    public void OnFocusOn()
    {
        try
        {
            if (CoreServices.InputSystem.GazeProvider.GazeTarget)
            {
                string parentULD = this.transform.parent.name;
                string item = this.name;

                Log(" GAZE: " + item + " " + parentULD);
            }
        }
        catch (Exception e)
        {

        }
    }

    public void OnManipulationStarted()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM OnManipulationStarted: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnManipulationsEnded()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM OnManipulationEnded: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnRemove()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-REMOVE: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnRemoveConfirm()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-REMOVE PROMPT CONFIRMED: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnRemoveCancel()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-REMOVE PROMPT CANCELLED: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnMove(string destinationULD="")
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-MOVE: " + item + " " + parentULD + " DESTINATION ULD: " + destinationULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnMoveConfirm(string destinationULD = "")
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-MOVE PROMPT CONFIRMED: " + item + " " + parentULD+ " DESTINATION ULD: "+ destinationULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnMoveCancel()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM ACTION-MOVE PROMPT CANCELLED: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }

    public void OnResponseSuccess()
    {
        try
        {
            Log(" RESPONSE SUCCESS");
        }
        catch (Exception e)
        {

        }
    }

    public void OnResponseFail()
    {
        try
        {
            Log(" RESPONSE FAIL");
        }
        catch (Exception e)
        {

        }
    }

    public void ItemReturned()
    {
        try
        {
            string parentULD = this.transform.parent.name;
            string item = this.name;

            Log(" ITEM RETURNED: " + item + " " + parentULD);
        }
        catch (Exception e)
        {

        }
    }
}
