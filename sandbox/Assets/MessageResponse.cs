﻿using Assets.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets
{
    public class MessageResponse
    {
        public string response;
        public string piece_unique_name; //only used (on the hololens side) in the case where the solver fails. Passing back the pieces name allows us to return it to its initial position.
        public string ULD_unique_name; // the FROM container the piece has been moved/removed from.
        public List<ULDflat> ulds;
    }
}
