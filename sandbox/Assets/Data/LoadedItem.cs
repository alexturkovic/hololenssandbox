﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Data
{
    // An item and its dimensions and position within the ULD.
    [Serializable]
    public class LoadedItem
    {
        public int height;  // item height
        public int lat;     // item width
        public int lng;     // item length
        public string piece; //it's name
        public string piece_unique_name; //annoying the piece names are often repeated. This unique 'piece_unique_name' is simply the piece name with a count (the count of the order its processed in) concatenated to the end of the piece name.
        public string shipment;
        public int start_height;
        public int start_lat;
        public int start_lng;
        public int offload_penalty;
        public int weight;
    }
}
