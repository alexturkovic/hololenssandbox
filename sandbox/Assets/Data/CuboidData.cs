﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets
{
    [Serializable]
    public class CuboidData
    {
        // variables only supported by Unity Serializer, no properties
        public float version;
        public int cuboidsTotal;
        public Cuboid[] cuboids;

        [Serializable]
        public class Cuboid
        {
            public string id;
            public float weight;
            public bool gravity;
            public XYZ position;
            public XYZ rotation;
            public XYZ localScale;

            [Serializable]
            public class XYZ
            {
                public float x;
                public float y;
                public float z;
            }
        }
    }
}
