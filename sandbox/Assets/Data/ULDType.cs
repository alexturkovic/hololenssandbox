﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Data
{
    public class ULDType
    {
        public static void GetULD(string jsonData)
        {
            JObject allObjects = JsonConvert.DeserializeObject<JObject>(jsonData);
        }
    }

    [Serializable]
    public class ULD
    {
        public string inner_lng_size;
        public string inner_lat_size;
        public string inner_height;
        public string max_weight;

    }
}
