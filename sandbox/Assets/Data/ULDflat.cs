﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Data
{
    public class ULDflat
    {
        public string ULD_name;
        public string ULD_unique_name;  // annoyingly some ULD names are the same as other ULD's. However the ULD name is unique to a segment. This 'ULD_unique_name' variable is a concatentation of the ULD segment and the ULD name.
        public string ULD_type;
        public string ULD_segment;
        public int ULD_total_weight;
        public string flight_leg_loading_position;
        public string flight_leg;
        public int flight_sequence;

        public List<LoadedItem> loadedItems = new List<LoadedItem>();

    }
}
