﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Assets.Data
{
    public class AircraftLayout
    {
        public static Dictionary<string, ULDPosition> ULDpositions = new Dictionary<string, ULDPosition>();

        public static Dictionary<string, ULDPosition> GetULDPositions(string jsonData)
        {
            JObject allObjects = JsonConvert.DeserializeObject<JObject>(jsonData);

            foreach(KeyValuePair<string, JToken> sub_obj in (JObject)allObjects)
            {
                ULDPosition uldPosition = new ULDPosition();
                uldPosition = JsonConvert.DeserializeObject<ULDPosition>(sub_obj.Value.ToString());
                ULDpositions.Add(sub_obj.Key, uldPosition);
            }

            return ULDpositions;
        }
    }

    [Serializable]
    public class ULDPosition
    {
        public int height_arm;
        public int lat_arm;
        public int lng_arm;
        public int width;
        public int depth;
        public int max_weight;
        public int rot;
    }
}
