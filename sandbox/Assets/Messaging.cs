﻿using Assets.Data;
using Microsoft.MixedReality.Toolkit.Experimental.Dialog;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using NativeWebSocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets
{
    public class Messaging : MonoBehaviour
    {
        private WebSocket websocket;

        /// <summary>
        /// 
        /// </summary>
        async void Start()
        {
            // kenobi
            // websocket = new WebSocket("ws://192.168.1.101:5001");

            // guidos machine
            // websocket = new WebSocket("ws://60.241.87.119:505");
            // websocket = new WebSocket("ws://192.168.1.101:5000");   // KENOBI WIFI HOME
            // websocket = new WebSocket("ws://192.168.1.12:5000");    // LAPTOP WIFI HOMO
            // websocket = new WebSocket("ws://192.168.0.167:5000");   // Portable WIFI
            // websocket = new WebSocket("ws://115.146.84.97:80");     // Nectar (original): https://nectar.org.au/
            websocket = new WebSocket("ws://115.146.85.44:80");     // Nectar (upgraded): https://nectar.org.au/


            //websocket = new WebSocket("ws://localhost:5001");

            websocket.OnOpen += () =>
            {
                Debug.Log("Connection open!");
            };

            websocket.OnError += (e) =>
            {
                Debug.Log("Error! " + e);
            };

            websocket.OnClose += (e) =>
            {
                Debug.Log("Connection closed! " + e.ToString());
            };

            websocket.OnMessage += (bytes) =>
            {
                Debug.Log("OnMessage!");
                //Debug.Log(bytes);

                // getting the message as a string
                var message = System.Text.Encoding.UTF8.GetString(bytes);
                //Debug.Log("OnMessage! " + message);
                ActionMessage(message);
            };

            // Keep sending messages at every 0.3s
            // InvokeRepeating("SendWebSocketMessage", 0.0f, 0.3f);

            // waiting for messages
            await websocket.Connect();
        }

        /// <summary>
        /// Actions a received message from the server. It first determines what type of message it is, then actions it.
        /// </summary>
        /// <param name="message">The message received from the server.</param>
        private void ActionMessage(string message)
        {
            // there will be 2 different types of messages, either the initial JSON data, or the updat to a ULD
            // but for now, there's only 1
            try
            {
                var data = JsonConvert.DeserializeObject<dynamic>(message);

                CreateCube createCube = GameObject.Find("NearMenu4x2").GetComponent<CreateCube>();

                if (data is JArray) // If it's an array, it's the initial data. ALL the data/ULD's/items etc.
                {
                    createCube.Create(message);
                }
                else if(data is System.Object)
                {
                    // Progress 3/4 - Update loading bar, which we're assuming exists
                    ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                    progressIndicator.UpdateSubText("Receiving...");
                    progressIndicator.UpdateProgress(0.5f);

                    createCube.UpdateULDs(message);
                }
                else
                {
                    throw new Exception("Could not read data at all, not an array or object");
                }
            }
            catch(Exception e)
            {
                Debug.Log("Error reading JSON message from server."+e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async void SendWebSocketMessage()
        {
            if (websocket.State == WebSocketState.Open)
            {
                // Sending bytes
                //await websocket.Send(new byte[] { 10, 20, 30 });

                // Sending plain text
                //await websocket.SendText("plain text message");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async void SendLoggingMessage(string text)
        {
            if (websocket.State == WebSocketState.Open)
            {
                try
                {
                    // Sending plain text
                    await websocket.SendText("LOG " + text);
                }
                catch(Exception e)
                {
                    Debug.Log("Messaging: Could not send logging message. "+e.Message);
                }
            }
        }

        public async void SendRemoveItemMessage(GameObject item)
        {
            if (websocket.State == WebSocketState.Open)
            {
                try
                {
                    Debug.Log("Removing item: " + item.name);

                    // Get the items name
                    ItemData itemData = item.GetComponent<ItemData>();

                    Message remove = new Message();
                    remove.action = "move";
                    remove.piece_unique_name = itemData.Piece_unique_name;
                    remove.from.ULD_unique_name = item.transform.parent.gameObject.name; // get the items parent
                    remove.to.ULD_unique_name = "Staging Area";

                    string json = JsonConvert.SerializeObject(remove);

                    await websocket.SendText(json);
                    //item.SetActive(false);

                    // Progress 2/4 - Show the progress bar
                    ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                    progressIndicator.UpdateSubText("Solving...");
                    progressIndicator.UpdateProgress(0.25f);
                }
                catch (Exception e)
                {
                    Debug.Log("ERROR: Could not either find the item to remove or there was a problem sending the message.");
                }
            }
        }

        public async void SendMoveItemMessage(GameObject item, string old_parent, string new_parent)
        {
            if (websocket.State == WebSocketState.Open)
            {
                try
                {
                    Debug.Log("Moving item: " + item.name);

                    // Get the items name
                    ItemData itemData = item.GetComponent<ItemData>();

                    Message move = new Message();
                    move.action = "move";
                    move.piece_unique_name = itemData.Piece_unique_name;
                    move.from.ULD_unique_name = old_parent;
                    move.to.ULD_unique_name = new_parent;

                    string json = JsonConvert.SerializeObject(move);

                    await websocket.SendText(json);

                    // Progress 2/4 - Show the progress bar
                    ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                    progressIndicator.UpdateSubText("Solving...");
                    progressIndicator.UpdateProgress(0.25f);

                    //item.SetActive(false);
                }
                catch (Exception e)
                {
                    Debug.Log("ERROR: Could not either find the item to remove or there was a problem sending the message.");
                }
            }
        }

        public void Update()
        {
            #if !UNITY_WEBGL || UNITY_EDITOR
                    websocket.DispatchMessageQueue();
            #endif
        }

        /// <summary>
        /// 
        /// </summary>
        private async void OnApplicationQuit()
        {
            await websocket.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public async void sendInit()
        {
            try
            {
                if (websocket.State == WebSocketState.Open)
                {
                    await websocket.SendText("init");
                    UpdateNearMenuAfterInit();
                }
                else
                {
                    Debug.Log("WebSocketState not open, trying again...");
                    // try to connect again
                    this.Start();
                    await websocket.SendText("init");
                    UpdateNearMenuAfterInit();
                }
            }
            catch (Exception e)
            {
                Debug.Log("Messaging could not send the init message.");
            }
        }

        private void UpdateNearMenuAfterInit()
        {
            try
            {
                GameObject btnCollection = GameObject.Find("ButtonCollection");
                
                GameObject btnInit = GameObject.Find("Init");
                GameObject btnRevert = btnCollection.transform.Find("Revert").gameObject; //https://answers.unity.com/questions/926399/find-inactive-game-object.html

                btnInit.active = false;
                btnRevert.active = true;

                // Update the collection
                GridObjectCollection collection = btnCollection.GetComponent<GridObjectCollection>();
                collection.UpdateCollection();
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}
