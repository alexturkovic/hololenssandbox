﻿using System.IO;
using UnityEngine;
using NativeWebSocket;
using Newtonsoft.Json;
using Assets;
using System;
using Assets.Data;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Experimental.Dialog;
using TMPro;
using Newtonsoft.Json.Linq;
using Random = UnityEngine.Random;
using System.Threading;
using System.Linq;

public class CreateCube : MonoBehaviour
{
    private string cuboidDataPath = Application.streamingAssetsPath + "/base/test_output.json";
    private string ULDpositionsDataPath = Application.streamingAssetsPath + "/masterdata/ac.json";

    Dictionary<string, ULDPosition> aircraftLayoutPositions;

    private List<ULDflat> data;
    private float initial_scale = 0.01f;
    private float post_processing_scale = 0.025f;

    // The sequence we want to read from the data
    int set_sequence = 1;

    private void Create()
    {
        GameObject nearMenu = GameObject.Find("NearMenu4x2");

        // Instantiate and show loading bar
        ProgressIndicator progressIndicator = nearMenu.GetComponent<ProgressIndicator>();
        progressIndicator.Init("Rendering", "Initialising...");

        // dont need to read all the data if its already sent to us form websocjet
        //string jsonString = File.ReadAllText(cuboidDataPath);
        int itemCount = 0; // a count of ALL items that are created
        int uldCount = 0; // a count of how many ULDs have been created

        // Get the flight schedule data  - don't need to do this if Create(json) is called
        // data = GetData(jsonString);
        
        Debug.Log("Total ULDs to draw: " + data.Count);

        // Get the aircrafts ULD positions
        string ULDpositionsDataString = File.ReadAllText(ULDpositionsDataPath);

        if (aircraftLayoutPositions == null)
        {
            aircraftLayoutPositions = AircraftLayout.GetULDPositions(ULDpositionsDataString);
        }

        // Draw all the items in the ULD, first, get the ULD
        foreach (ULDflat uld in data)
        {
            if(uld.flight_sequence == set_sequence) //TODO - we'll do other sequences later
            {
                uldCount++;
                itemCount += uld.loadedItems.Count;
                progressIndicator.UpdateSubText("ULD " + uldCount.ToString() + " of " + data.Count);

                CreateAndPositionULD(uld);
            }
        }

        progressIndicator.UpdateSubText("Calculating total weight");
        CalcTotalWeightAndProfit();

        Debug.Log("Total ULDs drawn: " + uldCount);
        Debug.Log("Total items drawn: " + itemCount);

        // Loading bar
        progressIndicator.UpdateSubText("Completed!");
        progressIndicator.Destroy(0.5f);

        // Set the colours
        CubeDataColours cdc = nearMenu.GetComponent<CubeDataColours>();
        cdc.RestoreColours();
    }

    /// <summary>
    /// Creates, scales, and positions an individual ULD.
    /// </summary>
    /// <param name="uld"></param>
    private void CreateAndPositionULD(ULDflat uld)
    {
        //Create a game object for each ULD, this will be used as a parent for all the container items for use in resizing and positioning.
        GameObject uld_container = Instantiate(Resources.Load("ULD_Container") as GameObject);
        uld_container.name = uld.ULD_unique_name;

        // A list of all items created for a single ULD
        List<GameObject> allItemsAsCuboids = new List<GameObject>();

        foreach (LoadedItem item in uld.loadedItems)
        {
            // new: https://answers.unity.com/questions/523289/change-size-of-mesh-at-runtime.html
            GameObject cuboid = Instantiate(Resources.Load("CuboidPrefabNoTheme") as GameObject);
            Mesh cuboidMesh = cuboid.GetComponent<MeshFilter>().mesh;

            // Add the weight and priority to the gameobject (makes it easier to get data from later on)
            ItemData itemData = cuboid.GetComponent<ItemData>();
            itemData.Offload_penalty = item.offload_penalty;
            itemData.Weight = item.weight;
            itemData.Piece_unique_name = item.piece_unique_name;

            // Resize the mesh of the prefab
            Vector3[] prefabsVertices = cuboidMesh.vertices;

            var vertices = new Vector3[prefabsVertices.Length];
            // LOGGING //Debug.Log("Number of vertices in prefab: " + prefabsVertices.Length);

            // Resize the prefab to the size of the item from the data
            for (int j = 0; j < vertices.Length; j++)
            {
                var vertex = prefabsVertices[j];

                vertex.x = vertex.x * item.lat;
                vertex.y = vertex.y * item.height;
                vertex.z = vertex.z * item.lng;
                vertices[j] = vertex;
            }

            cuboidMesh.vertices = vertices;
            cuboidMesh.RecalculateNormals();
            cuboidMesh.RecalculateBounds();

            // Resize the Box Collider mesh, so we can interact with it
            BoxCollider boxCollider = cuboid.GetComponent<BoxCollider>();
            boxCollider.size = cuboidMesh.bounds.size;

            //LOGGING
            Bounds bounds = cuboidMesh.bounds;
            //Debug.Log("Cuboid " + count);
            //Debug.Log("bounds.x: " + bounds.size.x);
            //Debug.Log("bounds.y: " + bounds.size.y);
            //Debug.Log("bounds.z: " + bounds.size.z);

            // Position the cuboid according to the position data
            cuboid.transform.position = new Vector3(item.start_lat + (bounds.size.x / 2), item.start_height + (bounds.size.y / 2), item.start_lng + (bounds.size.z / 2));
            cuboid.name = item.piece_unique_name;

            // Add gravity
            //cuboid.AddComponent<Rigidbody>();
            //cuboid.GetComponent<Rigidbody>().useGravity = true;

            allItemsAsCuboids.Add(cuboid);
        }

        if(uld.ULD_unique_name != "Staging Area") // If it's not in staging it's in a ULD
        {
            // Before we set all the ULD's items to the parent, we need to resize the (invisible) uld container
            Mesh uld_container_mesh = uld_container.GetComponent<MeshFilter>().mesh;
            Bounds uld_container_bounds = uld_container_mesh.bounds;

            switch (uld.ULD_type)
            {
                case "ake":
                    Vector3 ake_mesh_size = new Vector3(195, 153, 144); // size of the ake ULD in cm.
                    uld_container.transform.localScale = ake_mesh_size;
                    Bounds ake_bounds = uld_container_mesh.bounds;
                    ake_bounds.size = ake_mesh_size;
                    break;
                case "pmc_md11f_md":
                case "pmc_md11f_md_cad":
                    Vector3 md_mesh_size = new Vector3(243, 244, 317); // size of the md ULD in cm.
                    uld_container.transform.localScale = md_mesh_size;
                    Bounds md_bounds = uld_container_mesh.bounds;
                    md_bounds.size = md_mesh_size;
                    break;
                case "pmc_F_ld":
                    Vector3 ld_mesh_size = new Vector3(405, 153, 243); // size of the ld ULD in cm.
                    uld_container.transform.localScale = ld_mesh_size;
                    Bounds ld_bounds = uld_container_mesh.bounds;
                    ld_bounds.size = ld_mesh_size;
                    break;
                default:
                    break;
            }

            // Set all the created items to have the same parent
            foreach (GameObject item in allItemsAsCuboids)
            {
                // Adjust the position of each item to line up to the base of the ULD
                switch (uld.ULD_type)
                {
                    case "ake":
                        item.transform.localPosition = new Vector3(item.transform.position.x - 97.5f, item.transform.position.y - 76.5f, item.transform.position.z - 72f);
                        break;
                    case "pmc_md11f_md":
                    case "pmc_md11f_md_cad":
                        item.transform.localPosition = new Vector3(item.transform.position.x - 121.5f, item.transform.position.y - 122f, item.transform.position.z - 158.5f);
                        break;
                    case "pmc_F_ld":
                        item.transform.localPosition = new Vector3(item.transform.position.x - 202.5f, item.transform.position.y - 76.5f, item.transform.position.z - 121.5f);
                        break;
                    default:
                        break;
                }

                // Add the item to the parent (invisible) container
                item.transform.SetParent(uld_container.transform);
            }

            // Scale ALL items, ULD (invisible) container to match the ULD wireframe scaling
            uld_container.transform.localScale = uld_container.transform.localScale * initial_scale;

            // Add the ULD wireframe to the parent (invisible) ULD container
            try
            {
                switch (uld.ULD_type)
                {
                    case "ake":
                        GameObject ULD_wireframe_ake = Instantiate(Resources.Load("ULDs/uld_ake_GO") as GameObject);

                        ULD_wireframe_ake.transform.SetParent(uld_container.transform);
                        break;
                    case "pmc_md11f_md":
                    case "pmc_md11f_md_cad":
                        GameObject ULD_wireframe_md = Instantiate(Resources.Load("ULDs/uld_md_pmc_GO") as GameObject);

                        // FIX: the container wireframe was created to always look to the left (looking face on, from the nose of the aircraft to the tail)
                        ULD_wireframe_md.transform.Rotate(0, 180, 0);

                        // Add it to the parent (invisible) container
                        ULD_wireframe_md.transform.SetParent(uld_container.transform);
                        break;
                    case "pmc_F_ld":
                        GameObject ULD_wireframe_ld = Instantiate(Resources.Load("ULDs/uld_ld_pmc_GO") as GameObject);

                        // Add it to the parent (invisible) container
                        ULD_wireframe_ld.transform.SetParent(uld_container.transform);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.Log("Could not find ULD wireframe prefab.");
            }

            // Scale ALL items, ULD (invisible) container. AGAIN.
            uld_container.transform.localScale = uld_container.transform.localScale * post_processing_scale;

            // move the uld to its position in the aircraft
            if (uld.flight_leg_loading_position == null)
            {
                Debug.Log("Missing comparment name for ULD: " + uld.ULD_name);
            }
            ULDPosition position = aircraftLayoutPositions[uld.flight_leg_loading_position];

            //FIX
            if (uld.flight_leg_loading_position.EndsWith("L"))
            {
                //rotate the whole container
                uld_container.transform.Rotate(0, 180, 0);
            }

            //debugging
            /*
            if (uld.ULD_name != "pmc_md11f_md-1")
            {
                uld_container.active = false;
            }*///end debugging
            Vector3 vPosition = new Vector3(position.lat_arm * initial_scale * post_processing_scale, position.height_arm * initial_scale * post_processing_scale -0.065f, position.lng_arm * initial_scale * post_processing_scale + 0.5f);
            uld_container.transform.position = vPosition;

            // Rotate the ULD if 'rot' (rotation) is set
            if (position.rot == 1)
            {
                uld_container.transform.Rotate(0f, -90f, 0f, Space.Self);
            }

            // add to the aircraft body
            //uld_container.transform.SetParent(GameObject.Find("Aircraft").transform);

            // Tag the ULD so we can easily find them all later
            uld_container.tag = "ULD";
            //Debug.Log("CreateAndPositionULD END: " + allItemsAsCuboids.Count + "CONTAINER NAME: "+uld_container.name + "CONTAINER CHILD COUNT: "+uld_container.transform.childCount);
        }
        else // We just need to resize the items, and add them to the staging area
        {
            // Set all the created items to have the same parent
            foreach (GameObject item in allItemsAsCuboids)
            {
                // Add the item to the parent (invisible) container
                item.transform.SetParent(uld_container.transform);
            }

            // Scale ALL items in the ULD (invisible) container
            uld_container.transform.localScale = uld_container.transform.localScale * initial_scale * post_processing_scale;

            // move it to staging
            // Set all the created items to have the same parent
            foreach (GameObject item in allItemsAsCuboids)
            {
                item.GetComponent<Actions>().AddToStagingArea();
            }

            // we no longer need the container
            Destroy(uld_container);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="jsonData"></param>
    public void Create(string jsonData)
    {
        try
        {
            if (data != null)
            {
                data.Clear();
            }

            data = JsonConvert.DeserializeObject<List<ULDflat>>(jsonData);
            Create();
        }
        catch(Exception e)
        {
            Debug.Log("CreateCube: Could not deserialize the init JSON data."+e.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="jsonData"></param>
    public void UpdateULDs(string jsonData)
    {
        try
        {
            MessageResponse actionResponse = JsonConvert.DeserializeObject<MessageResponse>(jsonData);

            GameObject nearMenu = GameObject.Find("NearMenu4x2");

            // Progress 4/4 - Update loading bar, which we're assuming exists
            ProgressIndicator progressIndicator = nearMenu.GetComponent<ProgressIndicator>();
            progressIndicator.UpdateSubText("Updating...");
            progressIndicator.UpdateProgress(0.75f);

            // for colouring the items
            CubeDataColours cdc = nearMenu.GetComponent<CubeDataColours>();
            
            CargoLogging cargoLogging = nearMenu.GetComponent<CargoLogging>();

            if (actionResponse.response == "success")
            {
                cargoLogging.OnResponseSuccess();

                // 1 = REMOVE action. Only 1 container has been updated, which means only one item has been removed. OR this is a move from the staging area to a ULD.
                // 2 = MOVE action. 2 containers are in the data, means a piece was moved from one to the other.
                if (actionResponse.ulds.Count == 1)
                {
                    WeightProfit weightProfit = GameObject.Find("Weight And Profit").GetComponent<WeightProfit>();

                    foreach (ULDflat ULD_flat in actionResponse.ulds) // there should be only 1 since its a remove action, but still, its in an array
                    {
                        // Remove the weight and profit of the ULD from the current total (only if the item/piece has been removed).
                        int[] intitial_weight_and_profit = weightProfit.CalcULDWeightProfitTotal(ULD_flat.ULD_unique_name);
                        weightProfit.SubtractWeightOrProfit(intitial_weight_and_profit[0], intitial_weight_and_profit[1]);

                        // Then move the piece to the staging area (because its parent is about to be destroyed).
                        if (actionResponse.ULD_unique_name != "Staging Area")
                        {
                            Actions actions = GameObject.Find(actionResponse.piece_unique_name).GetComponent<Actions>();
                            actions.AddToStagingArea();
                        }

                        // Get the current position, rotation, and scale of the container

                        // Remove the current ULD and all items child items
                        GameObject parentContainer = GameObject.Find(ULD_flat.ULD_unique_name);

                        foreach (Transform item in parentContainer.transform)
                        {
                            DestroyImmediate(item.gameObject);
                        }
                        DestroyImmediate(parentContainer);


                        // this is totally a hack, but if this happened to be a move from 'Staging Area' to a ULD, then we need to delete the item from
                        // the staging area first
                        if (actionResponse.ULD_unique_name == "Staging Area")
                        {
                            DestroyImmediate(GameObject.Find(actionResponse.piece_unique_name));
                        }

                        // DEBUGGING
                        /*
                        try
                        {
                            var objs = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == ULD_flat.ULD_unique_name);

                            int x = objs.Count<GameObject>();

                            if(x != null)
                            {
                                Debug.Log("GAMEOBJECTS FOUND = "+x);
                            }
                        }
                        catch (Exception e)
                        {
                        }
                        */

                        // Create the new ULD
                        CreateAndPositionULD(ULD_flat);

                        // TODO Set the position, rotation, and scale of the container

                        // Update the weight, figure out the sum of the container, add it the total (only if the item/piece has been removed).
                        int[] updated_weight_and_profit = weightProfit.CalcULDWeightProfitTotal(ULD_flat.ULD_unique_name);
                        weightProfit.AddWeightOrProfit(updated_weight_and_profit[0], updated_weight_and_profit[1]);
                    }

                    progressIndicator.UpdateProgress(1f);
                    progressIndicator.Destroy(0.5f);
                }
                else if(actionResponse.ulds.Count == 2)
                {
                    foreach (ULDflat ULD_flat in actionResponse.ulds)
                    {
                        // Get the current position, rotation, and scale of the container

                        // Remove the current ULD
                        Destroy(GameObject.Find(ULD_flat.ULD_unique_name));

                        // Create the new ULD
                        CreateAndPositionULD(ULD_flat);

                        // TODO Set the position, rotation, and scale of the container
                    }
                    progressIndicator.UpdateProgress(1f);
                    progressIndicator.Destroy(0.5f);
                }
                else
                {
                    progressIndicator.Destroy(0.5f);
                    throw new Exception("Could not determine the type of reponse. Number of ULDs == "+actionResponse.ulds.Count.ToString());
                }

                cdc.RestoreColours();
            }
            else if(actionResponse.response == "fail")
            {
                cargoLogging.OnResponseFail();

                progressIndicator.UpdateProgress(1f);
                progressIndicator.Destroy(0.5f);

                // return the item back to its previous parent and position
                // get the action script for the piece:
                Actions actions_script = GameObject.Find(actionResponse.piece_unique_name).GetComponent<Actions>();
                actions_script.ShowFailedModal();
                actions_script.ReturnItemToIntialPosition();

                // TODO handle failure, display a message or something to the user
                Debug.Log("actionResponse == fail");

                //TODO display a modal that says failed and why
            }
        }
        catch (Exception e)
        {
            Debug.Log("CreateCube: Error during update."+e.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="jsonString"></param>
    /// <returns></returns>
    private List<ULDflat> GetData(string jsonString)
    {
        var allULDs = JsonConvert.DeserializeObject<List<ULDflat>>(jsonString);

        return allULDs;
    }

    private void CalcTotalWeightAndProfit()
    {
        try
        {
            GameObject GO_weight_profit = GameObject.Find("Weight And Profit");

            WeightProfit weightProfit = GO_weight_profit.GetComponent<WeightProfit>();
            weightProfit.CalcTotalWeightAndProfitTotal();
        }
        catch (Exception e)
        {

        }
    }

    // Assumes Create() has been executed
    public void Refresh()
    {
        /*
        foreach(GameObject uld in allULDs)
        {
            try
            {
                Destroy(uld);
            }
            catch(Exception e)
            {
                Debug.Log("Could not find uld to remove: "+ uld.name);
            }
        }

        Create();    
        */
    }

    // public void Awake() {}

    // Start is called before the first frame update
    // public void Start() {}

    // Update is called once per frame / 60 x a second
    // public void Update() {}
}
 