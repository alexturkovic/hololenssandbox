﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets
{
    public class Message
    {
        public string action;
        public string piece_unique_name;

        [JsonProperty(ItemIsReference = true)]
        public From from = new From();

        [JsonProperty(ItemIsReference = true)]
        public To to = new To();
    }

    public class From
    {
        public string ULD_unique_name { get; set; }
    }

    public class To
    {
        public string ULD_unique_name { get; set; }
    }
}
