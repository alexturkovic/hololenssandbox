﻿using Microsoft.MixedReality.Toolkit.Experimental.Dialog;
using System;
using System.Collections.Generic;
using System.Linq;
using NativeWebSocket;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using Newtonsoft.Json;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine.UI;

namespace Assets
{
    public class Actions : MonoBehaviour
    {
        private Messaging messaging;
        private CargoLogging cargoLogging;
        private Collider item_collider;
        private Vector3 item_initial_position;
        private Quaternion item_initial_rotation;

        /// <summary>
        /// Records the position of the item/piece at the beginning of the interaction, called by the "On Manipulation Started" event on the piece.
        /// </summary>
        public void RecordInitialPosition()
        {
            item_initial_position = this.transform.position;
            item_initial_rotation = this.transform.rotation;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteAction()
        {
            this.DetectActionType();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DetectActionType()
        {
            // get the item/pieces collider
            //item_collider = this.GetComponent<Collider>();
            Vector3 item_position = this.transform.position;

            // Get the parent ULD and its bounds
            GameObject item_parent = this.transform.parent.gameObject;
            Collider item_parent_collider = item_parent.GetComponent<Collider>();

            if (item_parent_collider.bounds.Contains(item_position)) // if the item has been moved but it's still within it's parent
            {
                // snap the item back to its original position
                ReturnItemToIntialPosition();
            }
            else // if the item has been moved outside of the parent ULD
            {
                // detect if its been moved to another ULD
                bool hasMovedToNewULD = false; // assume no for now

                GameObject[] uld_gameobjects = GameObject.FindGameObjectsWithTag("ULD");

                foreach (GameObject uld in uld_gameobjects)
                {
                    // get the ULDs collider
                    Collider uld_collider = uld.GetComponent<Collider>();

                    if (uld_collider.bounds.Contains(item_position))
                    {
                        // logging
                        cargoLogging.OnMove(uld.name);

                        // make its parent the new ULD and send the message over websocket
                        Dialog myDialog = Dialog.Open(Resources.Load("DialogSmall_192x96") as GameObject, DialogButtonType.Yes | DialogButtonType.No, "Move item?", "From ULD: " + item_parent.name + System.Environment.NewLine + "To ULD: " + uld.name, true, uld);

                        if (myDialog != null)
                        {
                            myDialog.OnClosed += OnMoveClosedDialogEvent;
                        }
                        hasMovedToNewULD = true;
                        break; // and quit the foreach, no point in continuing
                    }
                }

                if (!hasMovedToNewULD && item_parent.name != "Staging Area")
                {
                    // item has been removed from all ULDs and will be moved to the staging area
                    removeItem();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReturnItemToIntialPosition()
        {
            this.transform.position = item_initial_position;
            this.transform.rotation = item_initial_rotation;

            // unhighlight the parent
            CubeColour cc = this.GetComponent<CubeColour>();
            cc.UnhighlightParent();

            // log the return action
            cargoLogging.ItemReturned();
        }

        public async void removeItem()
        {
            try
            {
                // logging
                cargoLogging.OnRemove();

                // move to staging area
                ItemData itemData = this.GetComponent<ItemData>();
                Dialog myDialog = Dialog.Open(Resources.Load("DialogSmall_192x96") as GameObject, DialogButtonType.Yes | DialogButtonType.No, "Remove item from flight?", "Item weight: " + itemData.Weight + System.Environment.NewLine + "Item profit: " + itemData.Offload_penalty, true);

                if (myDialog != null)
                {
                    myDialog.OnClosed += OnRemoveClosedDialogEvent;
                }
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void OnRemoveClosedDialogEvent(DialogResult obj)
        {
            if (obj.Result == DialogButtonType.Yes)
            {
                Debug.Log(obj.Result);

                // logging
                cargoLogging.OnRemoveConfirm();

                //  Progress 1/4 - Instantiate and show loading bar
                ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                progressIndicator.Init("Update in progress", "Sending...");

                // Send the message (before we assign the new parent)
                messaging.SendRemoveItemMessage(this.gameObject);

                // update the total weight and profit
                // NOTE: No longer needed as this is subtracted from the weight in the CreateCube UpdateULDs method.
                /*
                GameObject GO_weight_and_profit = GameObject.Find("Weight And Profit");
                WeightProfit weightProfit = GO_weight_and_profit.GetComponent<WeightProfit>();
                ItemData itemData = this.GetComponent<ItemData>();
                weightProfit.SubtractWeightOrProfit(itemData.Weight, itemData.Offload_penalty);
                */
            }
            else
            {
                // logging
                cargoLogging.OnRemoveCancel();

                ReturnItemToIntialPosition();
            }

            // unhighlight the parent ULD
            CubeColour cc = this.GetComponent<CubeColour>();
            cc.UnhighlightParent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void OnMoveClosedDialogEvent(DialogResult obj)
        {
            if (obj.Result == DialogButtonType.Yes)
            {
                

                //  Progress 1/4 - Instantiate and show loading bar
                ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                progressIndicator.Init("Update in progress", "Sending...");

                // get the old parent
                string old_parent = this.transform.parent.gameObject.name;

                // get the new parent
                GameObject new_parent = obj.Variable as GameObject;

                // logging
                cargoLogging.OnMoveConfirm(new_parent.name);

                // send the message to the server
                messaging.SendMoveItemMessage(this.gameObject, old_parent, new_parent.name);

                // attach new parent
                // note: not doing this anymore as the server solver is sending the data back with the item under the new parent (should it be a success).
                // this.transform.SetParent(new_parent.transform);

                // add the weight if it's coming from the staging area
                // note: this is no longer required as the server solver is sending the data back with the item under the new parent (should it be a success).
            }
            else
            {
                // logging
                cargoLogging.OnMoveCancel();

                ReturnItemToIntialPosition();
            }

            // unhighlight the parent ULD
            CubeColour cc = this.GetComponent<CubeColour>();
            cc.UnhighlightParent();
        }

        /// <summary>
        /// Because even if the user has selected 'Yes' on the confirmation modal to remove the item from the ULD, we can only really remove it
        /// if the solver returns a success message. So this method is called if its a success, otherwise its initial ULD will remain its parent. 
        /// </summary>
        public void AddToStagingArea()
        {
            GameObject staging_area = GameObject.Find("Staging Area");

            // detach from parent
            this.transform.parent = null;

            // attach new parent
            this.transform.SetParent(staging_area.transform);

            // re-org the dock
            GridObjectCollection grid = staging_area.GetComponent<GridObjectCollection>();
            grid.UpdateCollection();
        }

        public void Start()
        {
            try
            {
                messaging = GameObject.Find("NearMenu4x2").GetComponent<Messaging>();
                cargoLogging = this.GetComponent<CargoLogging>();
            }
            catch(Exception e)
            {
                Debug.Log("error trying to get the messagin script off the NearMenu4x2");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="profit"></param>
        private void HUD_SetWeightProfit(string weight, string profit)
        {
            try
            {
                Text HUD_weight_value = GameObject.Find("HUD_Weight_Value").GetComponent<Text>();
                Text HUD_profit_value = GameObject.Find("HUD_Profit_Value").GetComponent<Text>();

                HUD_weight_value.text = weight;
                HUD_profit_value.text = profit;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        /// <summary>
        /// Hides the current value of the weight and profit from the HUD.
        /// </summary>
        public void HUD_HideWeightProfit()
        {
            try
            {
                HUD_SetWeightProfit("", "");
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        /// <summary>
        /// Gets the current items weight and profit, and updates the HUD display
        /// </summary>
        public void HUD_ShowWeightProfit()
        {
            try
            {
                ItemData itemData = this.GetComponent<ItemData>();

                string weight = itemData.Weight.ToString();
                string profit = itemData.Offload_penalty.ToString();

                HUD_SetWeightProfit(weight, profit);

            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        public async void revertVisualisation()
        {
            try
            {
                Dialog myDialog = Dialog.Open(Resources.Load("DialogSmall_192x96") as GameObject, DialogButtonType.Yes | DialogButtonType.No, "Revert?", "All changes will be lost.", true);

                if (myDialog != null)
                {
                    myDialog.OnClosed += OnRevertDialogEvent;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        private void OnRevertDialogEvent(DialogResult obj)
        {
            if (obj.Result == DialogButtonType.Yes)
            {
                //  Progress 1/4 - Instantiate and show loading bar
                ProgressIndicator progressIndicator = GameObject.Find("NearMenu4x2").GetComponent<ProgressIndicator>();
                progressIndicator.Init("Update in progress", "Destroying current visualisation...");

                progressIndicator.UpdateProgress(0.25f);
                progressIndicator.UpdateSubText("Resetting billboards...");
                WeightProfit weightProfit = GameObject.Find("Weight And Profit").GetComponent<WeightProfit>();
                weightProfit.ResetWeightProfitDisplays();

                // Destory container, uld item, uld wireframe
                GameObject[] uld_gameobjects = GameObject.FindGameObjectsWithTag("ULD");
                foreach (GameObject uld in uld_gameobjects)
                {
                    Destroy(uld);
                }

                // .. and from staging
                GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
                foreach(GameObject item in items)
                {
                    Destroy(item);
                }

                // Recreate everything
                progressIndicator.UpdateProgress(0.50f);
                progressIndicator.UpdateSubText("Creating...");
                messaging.sendInit();
                progressIndicator.UpdateProgress(1.0f);
                progressIndicator.UpdateSubText("Done...");
                progressIndicator.Destroy(0.5f);
            }
            // else do nothing
        }

        internal void ShowFailedModal()
        {
            Dialog myDialog = Dialog.Open(Resources.Load("DialogSmall_192x96") as GameObject, DialogButtonType.OK, "Failed to move item", "Not enough space available.", true);

            if (myDialog != null)
            {
                myDialog.OnClosed += OnSolverFail;
            }
        }

        private void OnSolverFail(DialogResult obj)
        {
            if (obj.Result == DialogButtonType.OK)
            {
                // DO nothing, the CreateCube.UpdateULDs() method returns the item to its position.
            }
        }
    }
}
