﻿using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets
{
    public class CubeDataColours : MonoBehaviour
    {
        private int minWeight;
        private int maxWeight;
        private int minPenalty; //profit
        private int maxPenalty;

        private int currentMode = 1;  // where 1=random colours, 2=weight, 3=profit/penaltya

        /// <summary>
        /// Colours ALL items in both staging and ULD's with a green colour according to their offload_penalty value.
        /// </summary>
        public void setPenaltyColour()
        {
            minPenalty = 0;
            maxPenalty = 0;
            int penaltyScale = 0;

            currentMode = 3;

            // get all the items
            GameObject[] items = GameObject.FindGameObjectsWithTag("Item");

            // figure out the scale of colour to colour each item
            foreach(GameObject item in items)
            {
                ItemData itemData = item.GetComponent<ItemData>();

                try
                {
                    int penalty = itemData.Offload_penalty;

                    if (minPenalty == 0 && maxPenalty == 0)
                    {
                        maxPenalty = penalty;
                    }
                    else if (minPenalty == 0 && maxPenalty > 0 && penalty > maxPenalty)
                    {
                        maxPenalty = penalty;
                    }
                    else if (minPenalty == 0 && maxPenalty > 0 && penalty < maxPenalty)
                    {
                        minPenalty = penalty;
                    }
                    else if (minPenalty > 0 && maxPenalty > 0 && penalty < maxPenalty && penalty < minPenalty)
                    {
                        minPenalty = penalty;
                    }
                    else if (minPenalty > 0 && maxPenalty > 0 && penalty > maxPenalty)
                    {
                        maxPenalty = penalty;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Child item that I can't get penalty from: " + item.name);
                }
            }

            penaltyScale = maxPenalty - minPenalty;

            foreach(GameObject item in items)
            {
                ItemData itemData = item.GetComponent<ItemData>();

                //Debug.Log("actual penalty: " + itemData.Weight + " minPenalty: " + minPenalty + " maxPenalty: " + maxPenalty + " penaltyScale: " + penaltyScale);

                float colorValue = ((float)itemData.Offload_penalty - (float)minPenalty) / penaltyScale;

                // Set the colour. Because bright green and red from the ULD wireframe hurt eyes, we need to turn down the saturation.
                CubeColour currentColour = item.GetComponent<CubeColour>();
                currentColour.UpdateColor(new Color(0 + colorValue, 1f, 0 + colorValue));
            }
        }

        /// <summary>
        ///  Colours ALL items in both staging and ULD's with a blue colour according to their weight value.
        /// </summary>
        public void setWeightColour()
        {
            minWeight = 0;
            maxWeight = 0;
            int colorScale = 0;

            currentMode = 2;

            // get all the items
            GameObject[] items = GameObject.FindGameObjectsWithTag("Item");

            foreach (GameObject item in items)
            {
                ItemData itemData = item.GetComponent<ItemData>();

                try
                {
                    int weight = itemData.Weight;

                    if (minWeight == 0 && maxWeight == 0)
                    {
                        maxWeight = weight;
                    }
                    else if (minWeight == 0 && maxWeight > 0 && weight > maxWeight)
                    {
                        maxWeight = weight;
                    }
                    else if (minWeight == 0 && maxWeight > 0 && weight < maxWeight)
                    {
                        minWeight = weight;
                    }
                    else if (minWeight > 0 && maxWeight > 0 && weight < maxWeight && weight < minWeight)
                    {
                        minWeight = weight;
                    }
                    else if (minWeight > 0 && maxWeight > 0 && weight > maxWeight)
                    {
                        maxWeight = weight;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Child item that I can't get weight from: " + item.name);
                }
            }

            colorScale = maxWeight - minWeight;

            foreach(GameObject item in items)
            {
                ItemData itemData = item.GetComponent<ItemData>();

                //Debug.Log("actual weight: " + itemData.Weight + " minWeight: " + minWeight + " maxWeight: " + maxWeight + " colorScale: " + colorScale);

                float colorValue = ((float)itemData.Weight - (float)minWeight) / colorScale;

                CubeColour currentColour = item.GetComponent<CubeColour>();
                currentColour.UpdateColor(new Color(colorValue, colorValue, 1f));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void setRandomColours()
        {
            currentMode = 1;

            // get all the items
            GameObject[] items = GameObject.FindGameObjectsWithTag("Item");

            foreach (GameObject item in items)
            {
                CubeColour cc = item.GetComponent<CubeColour>();
                cc.SetRandomColor();
            }
        }

        public void DisplayWeightFilter()
        {
            try
            {
                // get the parent
                GameObject weightFilter = GameObject.Find("NearMenu4x2").transform.Find("WeightFilterParent").gameObject;

                // activate the child
                if (!weightFilter.active)
                {
                    weightFilter.active = true;
                }

                //reset the child value
                PinchSlider slider = GameObject.Find("WeightPinchSlider").GetComponent<PinchSlider>();
                slider.SliderValue = 0;
            }
            catch (Exception e)
            {

            }
        }

        public void DisplayProfitFilter()
        {
            try
            {
                // get the parent
                GameObject profitFilter = GameObject.Find("NearMenu4x2").transform.Find("ProfitFilterParent").gameObject;

                // activate the child
                if (!profitFilter.active)
                {
                    profitFilter.active = true;
                }

                //reset the child value
                PinchSlider slider = GameObject.Find("ProfitPinchSlider").GetComponent<PinchSlider>();
                slider.SliderValue = 0;
            }
            catch (Exception e)
            {

            }
        }

        public void HideWeightFilter()
        {
            try
            {
                // get the parent
                GameObject weightFilter = GameObject.Find("NearMenu4x2").transform.Find("WeightFilterParent").gameObject;

                // activate the child
                if(weightFilter.active)
                {
                    weightFilter.active = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        public void HideProfitFilter()
        {
            try
            {
                // get the parent
                GameObject profitFilter = GameObject.Find("NearMenu4x2").transform.Find("ProfitFilterParent").gameObject;

                // activate the child
                if (profitFilter.active)
                {
                    profitFilter.active = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Requires setWeightColour() to have run first.
        /// </summary>
        public void FilterByWeight()
        {
            if(minWeight != null && maxWeight != null)
            {
                // reset the weight visuals
                setWeightColour();
                
                // get the sliders value
                PinchSlider slider = GameObject.Find("WeightPinchSlider").GetComponent<PinchSlider>();

                // slider values are from 0 to 1
                float sliderValue = slider.SliderValue;

                // get all items in ULD's only, not in Staging
                // Get the parent ULD container by unique name
                GameObject[] ULD_containers = GameObject.FindGameObjectsWithTag("ULD");

                foreach(GameObject ULD_container in ULD_containers)
                {
                    // Go through each of it's child objects (items and the wireframe)
                    foreach (Transform item in ULD_container.transform)
                    {
                        if (!item.name.StartsWith("uld")) // Need this check because there is at least 1 child object that is a wireframe and has no ItemData
                        {
                            ItemData itemData = item.GetComponent<ItemData>();

                            float itemScaled = (float)itemData.Weight / (float)maxWeight;

                            if (itemScaled<sliderValue)
                            {
                                CubeColour itemCC = item.GetComponent<CubeColour>();

                                Color color = Color.grey;
                                color.a = 0.001f;

                                itemCC.UpdateColor(color);
                                //item.GetComponent<Renderer>().material.color = col;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Requires setPenaltyColour() to have run first.
        /// </summary>
        public void FilterByProfit()
        {
            if (minPenalty != null && maxPenalty != null)
            {
                // reset the weight visuals
                setPenaltyColour();

                // get the sliders value
                PinchSlider slider = GameObject.Find("ProfitPinchSlider").GetComponent<PinchSlider>();

                // slider values are from 0 to 1
                float sliderValue = slider.SliderValue;

                // get all items in ULD's only, not in Staging
                // Get the parent ULD container by unique name
                GameObject[] ULD_containers = GameObject.FindGameObjectsWithTag("ULD");

                foreach (GameObject ULD_container in ULD_containers)
                {
                    // Go through each of it's child objects (items and the wireframe)
                    foreach (Transform item in ULD_container.transform)
                    {
                        if (!item.name.StartsWith("uld")) // Need this check because there is at least 1 child object that is a wireframe and has no ItemData
                        {
                            ItemData itemData = item.GetComponent<ItemData>();

                            float itemScaled = (float)itemData.Offload_penalty / (float)maxPenalty;

                            if (itemScaled < sliderValue)
                            {
                                CubeColour itemCC = item.GetComponent<CubeColour>();

                                Color color = Color.grey;
                                color.a = 0.001f;

                                itemCC.UpdateColor(color);
                                //item.GetComponent<Renderer>().material.color = col;
                            }
                        }
                    }
                }
            }
        }

        internal void RestoreColours()
        {
            switch(currentMode)
            {
                case 1: setRandomColours();
                    break;
                case 2: FilterByWeight();
                    break;
                case 3: FilterByProfit();
                    break;
                default: setRandomColours();
                    break;
            }
        }
    }
}