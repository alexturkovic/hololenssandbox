﻿using System;
using TMPro;
using UnityEngine;

namespace Assets
{
    public class WeightProfit : MonoBehaviour
    {
        private int totalItemWeight = 0;
        private int totalAvailableWeight = 0;
        private int totalProfit = 0;

        /// <summary>
        /// Calculatest the total weight and profit for ALL items in ALL ULDs.
        /// </summary>
        public void CalcTotalWeightAndProfitTotal()
        {
            // Get the weight of all the items to calculate a total
            // Get all the ULD's first
            GameObject[] all_uld_gameobjects = GameObject.FindGameObjectsWithTag("ULD");

            foreach (GameObject uld in all_uld_gameobjects)
            {
                int[] totalWeightAndProfit = CalcULDWeightProfitTotal(uld.name);

                if(totalWeightAndProfit[0] != -1 || totalWeightAndProfit[1] != -1)
                {
                    totalItemWeight += totalWeightAndProfit[0];
                    totalProfit += totalWeightAndProfit[1];
                }
            }

            InitWeightProfitDisplays(totalItemWeight, totalProfit);
        }

        /// <summary>
        /// Calculates the total weight and profit for all the items in a SINGLE specific ULD.
        /// </summary>
        /// <returns>The total weight (array element 0) and profit (array element 1) of all the items in the specified ULD. >=0 are valid values, -1 returned if there has been some kind of error.</returns>
        public int[] CalcULDWeightProfitTotal(string ULD_unique_name)
        {
            try
            {
                int ULD_total_weight = 0;
                int ULD_total_profit = 0;

                // Get the parent ULD container by unique name
                GameObject ULD_container = GameObject.Find(ULD_unique_name);

                // Go through each of it's child objects (items and the wireframe)
                foreach (Transform childItem in ULD_container.transform)
                {
                    if (!childItem.name.StartsWith("uld")) // Need this check because there is at least 1 child object that is a wireframe and has no ItemData
                    {
                        ItemData itemData = childItem.GetComponent<ItemData>();
                        ULD_total_weight += itemData.Weight;
                        ULD_total_profit += itemData.Offload_penalty;
                    }
                }

                return new int[2] { ULD_total_weight, ULD_total_profit };
            }
            catch(Exception e)
            {
                Debug.Log("WeightProfit: Error with calculating the ULDs total weight: " + e.Message);

                return new int[2] {-1,-1};
            }
        }

        /// <summary>
        /// Assumes the weight and profit have already been calculated (method: InitWeightProfitDisplays), otherwise it will just subtract it from 0. This method saves computing the entire weight and profit again. 
        /// It subtracts the weight from the Total Item Weight, and adds the amount to the Available Weight.
        /// </summary>
        /// <param name="weight">The weight to subtract from the total weight displayed.</param>
        /// <param name="profit">The profit to subtract from the total profit displayed.</param>
        public void SubtractWeightOrProfit(int weight, int profit)
        {
            try
            {
                if (weight > 0)
                {
                    // Get the text components of the display
                    GameObject displayItemWeightTotal = GameObject.Find("UpdatedTotalItemWeight");
                    GameObject displayAvailableWeightTotal = GameObject.Find("UpdatedTotalAvailableWeight");

                    TextMeshPro displayItemWeightTotal_text = displayItemWeightTotal.GetComponent<TextMeshPro>();
                    TextMeshPro displayAvailableWeightTotal_text = displayAvailableWeightTotal.GetComponent<TextMeshPro>();

                    totalItemWeight -= weight;
                    totalAvailableWeight += weight;

                    displayItemWeightTotal_text.text = String.Format("{0:n0}", totalItemWeight) + "kg";
                    displayAvailableWeightTotal_text.text = String.Format("{0:n0}", totalAvailableWeight) + "kg";
                }

                if (profit > 0)
                {
                    GameObject displayTotalProfit = GameObject.Find("UpdatedTotalProfit");
                    TextMeshPro displayTotalProfit_text = displayTotalProfit.GetComponent<TextMeshPro>();

                    this.totalProfit -= profit;
                    displayTotalProfit_text.text = this.totalProfit.ToString("C0");
                }
            }
            catch (Exception e)
            {
                Debug.Log("SubtractWeightOrProfit error.");
            }
        }

        /// <summary>
        /// Assumes the weight and profit have already been calculated (method: InitWeightProfitDisplays), otherwise it will just add it to 0. This method saves computing the entire weight and profit again. 
        /// It adds the weight to the Total Item Weight, and subtracts the amount from the Available Weight.
        /// </summary>
        /// <param name="weight">The weight to add from the total weight displayed.</param>
        /// <param name="profit">The profit to add from the total profit displayed.</param>
        public void AddWeightOrProfit(int weight, int profit)
        {
            try
            {
                if (weight > 0)
                {
                    // Get the text components of the display
                    GameObject displayItemWeightTotal = GameObject.Find("UpdatedTotalItemWeight");
                    GameObject displayAvailableWeightTotal = GameObject.Find("UpdatedTotalAvailableWeight");

                    TextMeshPro displayItemWeightTotal_text = displayItemWeightTotal.GetComponent<TextMeshPro>();
                    TextMeshPro displayAvailableWeightTotal_text = displayAvailableWeightTotal.GetComponent<TextMeshPro>();

                    totalItemWeight += weight;
                    totalAvailableWeight -= weight;

                    displayItemWeightTotal_text.text = String.Format("{0:n0}", totalItemWeight) + "kg";
                    displayAvailableWeightTotal_text.text = String.Format("{0:n0}", totalAvailableWeight) + "kg";
                }

                if (profit > 0)
                {
                    GameObject displayTotalProfit = GameObject.Find("UpdatedTotalProfit");
                    TextMeshPro displayTotalProfit_text = displayTotalProfit.GetComponent<TextMeshPro>();

                    this.totalProfit += profit;
                    displayTotalProfit_text.text = this.totalProfit.ToString("C0");
                }
            }
            catch (Exception e)
            {
                Debug.Log("AddWeightOrProfit error.");
            }
        }

        /// <summary>
        /// Initialises the 2 billboard displays for the initial weight and profit and the updated weight and profit.
        /// </summary>
        /// <param name="totalItemWeight"></param>
        /// <param name="totalProfit"></param>
        public void InitWeightProfitDisplays(int totalItemWeight, int totalProfit)
        {
            // Get the text component of the display for the INITIAL weight and profit billboard
            GameObject displayWeightTotal = GameObject.Find("TotalItemWeight");
            TextMeshPro displayWeightTotal_text = displayWeightTotal.GetComponent<TextMeshPro>();

            GameObject displayProfitTotal = GameObject.Find("TotalProfit");
            TextMeshPro displayProfitTotal_text = displayProfitTotal.GetComponent<TextMeshPro>();

            GameObject displayAvailableWeightTotal = GameObject.Find("TotalAvailableWeight");
            TextMeshPro displayAvailableWeightTotal_text = displayAvailableWeightTotal.GetComponent<TextMeshPro>();

            // Get the text component of the display for the UPDATED weight and profit billboard
            GameObject displayUpdatedWeightTotal = GameObject.Find("UpdatedTotalItemWeight");
            TextMeshPro displayUpdatedWeightTotal_text = displayUpdatedWeightTotal.GetComponent<TextMeshPro>();

            GameObject displayUpdatedProfitTotal = GameObject.Find("UpdatedTotalProfit");
            TextMeshPro displayUpdatedProfitTotal_text = displayUpdatedProfitTotal.GetComponent<TextMeshPro>();

            GameObject displayUpdatedAvailableWeightTotal = GameObject.Find("UpdatedTotalAvailableWeight");
            TextMeshPro displayUpdatedAvailableWeightTotal_text = displayUpdatedAvailableWeightTotal.GetComponent<TextMeshPro>();

            // Calculate the initial available weight
            int maxPayload = 91962;
            totalAvailableWeight = maxPayload - totalItemWeight;

            // Display the INITIAL billboard display values
            displayWeightTotal_text.text = String.Format("{0:n0}", totalItemWeight) + "kg";
            displayAvailableWeightTotal_text.text = String.Format("{0:n0}", totalAvailableWeight) + "kg";
            displayProfitTotal_text.text = totalProfit.ToString("C0");

            // Display the UPDATE billboard display values
            displayUpdatedWeightTotal_text.text = String.Format("{0:n0}", totalItemWeight) + "kg";
            displayUpdatedAvailableWeightTotal_text.text = String.Format("{0:n0}", totalAvailableWeight) + "kg";
            displayUpdatedProfitTotal_text.text = totalProfit.ToString("C0");
        }

        /// <summary>
        /// Sets the existing weight and profit displays to 0.
        /// </summary>
        public void ResetWeightProfitDisplays()
        {
            totalItemWeight = 0;
            totalAvailableWeight = 0;
            totalProfit = 0;
            InitWeightProfitDisplays(totalItemWeight, totalProfit);
        }
    }
}
